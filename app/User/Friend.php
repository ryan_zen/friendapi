<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{

    protected $fillable = [
        'req_user', 'acc_user', 'accepted_at', 'rejected_at'
    ];

    public function requser () {
        return $this->belongsTo('App\User', 'req_user', 'id');
    }

    public function accuser () {
        return $this->belongsTo('App\User', 'acc_user', 'id');
    }

}
