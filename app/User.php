<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\User\Friend;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];


    public function profile() {
        return $this->hasOne('App\User\Profile', 'userid', 'id');
    }

    public function req_friends () {
        return $this->hasMany('App\User\Friend', 'req_user', 'id')->with('accuser', 'accuser.profile');
    }

    public function acc_friends () {
        return $this->hasMany ('App\User\Friend', 'acc_user', 'id')->with(['requser', 'requser.profile']);
    }

    public function friendExist ($userFriend) {
        $userid = $this->id;
        $existed = Friend::where(function($query) use ($userid){
            $query->where('req_user', $userid)
                    ->orWhere('acc_user', $userid);
        })->where(function ($query) use ($userFriend){
            $query->where('req_user', $userFriend->id)
                    ->orWhere('acc_user', $userFriend->id);
        })->first();
        return $existed;
    }

    /**
     * Roll API Key
     */
    public function rollApiKey(){
       do{
          $this->api_token = str_random(60);
       }while($this->where('api_token', $this->api_token)->exists());
       $this->save();
    }

}
