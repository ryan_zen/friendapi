@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
           <user></user>
        </div>

        <div class="col-md-6">
           <user-profile></user-profile>
        </div>

        <div class="col-md-12">
           <friend-list></friend-list>
        </div>

    </div>
</div>
@endsection
