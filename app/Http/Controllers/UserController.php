<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
    public function index (Request $req) {
        $user = Auth::user();
        return $user->makeVisible('api_token')->toArray();
    }

    public function search (Request $req) {
        $user = Auth::user();
        $list = User::where('name', 'like', '%' . $req->text . '%')
            ->where('id', '!=', $user->id)
            ->with('profile')->get();
        return $list;
    }

}

