<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User\Profile;

class ProfileController extends Controller
{

    public function index (Request $req) {
        $user = Auth::user();
        return $user->profile;
    }

    public function edit (Request $req) {

        if ($req->id) {
            $profile = Profile::findOrFail($req->id);
        } else {
            $profile = new Profile();
        }

        $profile->fill($req->all());
        $profile->userid = Auth::user()->id;
        $profile->save();
        return $profile;
    }


    public function delete($profileId) {
        $profile = Profile::findOrFail($profileId);
        $profile->delete();
        return 'ok';
    }
}
