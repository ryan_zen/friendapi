<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $table = 'profiles';

    protected $fillable = [
        'description', 'hobby',
    ];


}
