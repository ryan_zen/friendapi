<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::middleware(['auth:api'])->group(function () {

    Route::post('/user', 'UserController@index');
    Route::get('/user/search', 'UserController@search');
    Route::post('/user/search', 'UserController@search');

    Route::post('/user/profile', 'User\ProfileController@index');
    Route::post('/user/profile/edit', 'User\ProfileController@edit');
    Route::delete('/user/profile/{id}', 'User\ProfileController@delete');

    Route::get('/friends', 'User\FriendController@index');
    Route::get('/friends/add/{acc_user}', 'User\FriendController@add');
    Route::get('/friends/delete/{friend_id}', 'User\FriendController@delete');

});

