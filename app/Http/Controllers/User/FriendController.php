<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User\Friend;
use App\User;

class FriendController extends Controller
{

    public function index() {
        $user = Auth::user();
        $user->req_friends;
        $user->acc_friends;
        return $user;
    }

    public function add($friend_id) {

        $user = Auth::user();
        $userFriend = User::findOrFail($friend_id);

        $existed = $user->friendExist($userFriend);

        if ($existed) {
            throw new \Exception ('Duplicate friend: ' . $userFriend->name);
        }


        $friend = new Friend([
            'req_user' => $user->id,
            'acc_user' => $userFriend->id,
        ]);
        $friend->save();
        $friend->accuser;

        return $friend;
    }

    public function delete($friend_id) {

        $user = Auth::user();
        $friend = Friend::findOrFail($friend_id);

        $friend->delete();
        return 'ok';

    }

}
